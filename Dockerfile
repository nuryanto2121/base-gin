FROM golang:latest

ENV GOPROXY https://goproxy.cn,direct
WORKDIR $GOPATH/src/gitlab.com/nuryanto2121/base-gin
COPY . $GOPATH/src/gitlab.com/nuryanto2121/base-gin
RUN go build .

EXPOSE 8000
ENTRYPOINT ["./go-gin-example"]
