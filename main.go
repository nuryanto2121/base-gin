package main

import (
	"fmt"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/nuryanto2121/base-gin/pkg/logging"
	"gitlab.com/nuryanto2121/base-gin/pkg/postgres"
	"gitlab.com/nuryanto2121/base-gin/pkg/redisdb"
	"gitlab.com/nuryanto2121/base-gin/pkg/setting"
	"gitlab.com/nuryanto2121/base-gin/routers"
)

func init() {
	setting.Setup()
	logging.Setup()
	postgres.Setup()
	redisdb.Setup()

}

// @title Base Gin
// @version 1.0
// @description Backend REST API for golang nuryanto2121

// @contact.name Nuryanto
// @contact.url https://www.linkedin.com/in/nuryanto-1b2721156/
// @contact.email nuryantofattih@gmail.com

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	gin.SetMode(setting.ServerSetting.RunMode)

	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)

	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(cors.Default())

	R := routers.GinRoutes{G: r}
	R.Init()
	r.Run(endPoint)
}
