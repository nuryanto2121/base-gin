package contfileupload

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	ifileupload "gitlab.com/nuryanto2121/base-gin/interface/fileupload"
	"gitlab.com/nuryanto2121/base-gin/models"
	"gitlab.com/nuryanto2121/base-gin/pkg/app"

	"gitlab.com/nuryanto2121/base-gin/pkg/file"
	"gitlab.com/nuryanto2121/base-gin/pkg/logging"
	tool "gitlab.com/nuryanto2121/base-gin/pkg/tools"
	util "gitlab.com/nuryanto2121/base-gin/pkg/utils"

	"github.com/gin-gonic/gin"
)

// ContFileUpload :
type ContFileUpload struct {
	useSaFileUpload ifileupload.UseCase
}

// NewContFileUpload :
func NewContFileUpload(e *gin.Engine, useSaFileUpload ifileupload.UseCase) {
	cont := &ContFileUpload{
		useSaFileUpload: useSaFileUpload,
	}

	e.Static("/wwwroot", "wwwroot")
	r := e.Group("/fileupload")
	// Configure middleware with custom claims
	// r.Use(midd.Versioning)
	// r.Use(midd.Authorize)
	r.POST("", cont.CreateImage)

}

// CreateImage :
// @Summary File Upload
// @Description Upload file
// @Tags FileUpload
// @Accept  multipart/form-data
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param upload_file formData file true "account image"
// @Param path formData string true "path images"
// @Success 200 {object} app.Response
// @Router /fileupload [post]
func (u *ContFileUpload) CreateImage(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}
	var (
		appE          = app.Gin{C: e}
		imageFormList []models.FileUpload
		logger        = logging.Logger{}
	)

	form, err := e.MultipartForm()
	if err != nil {
		appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}
	images := form.File["upload_file"]

	pt := form.Value["path"]

	logger.Info(pt)
	//directory api
	dir, err := os.Getwd()
	if err != nil {
		appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}

	for i, image := range images {
		// Source
		src, err := image.Open()
		if err != nil {
			appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
			return
		}
		defer src.Close()

		var dir_file = dir + "/wwwroot/uploads"
		var path_file = "/wwwroot/uploads"
		err = file.IsNotExistMkDir(dir_file)
		if err != nil {
			appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
			return
		}

		// create folder directory if not exist from param
		if pt[i] != "" {

			dirx := dir_file
			for _, val := range strings.Split(pt[i], "/") {
				dirx = dirx + "/" + val
				fmt.Printf("%v", dirx)
				err = file.IsNotExistMkDir(dirx)
				if err != nil {
					appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
					return
				}
			}
			dir_file = fmt.Sprintf("%s/%s", dir_file, pt[i])

			path_file = fmt.Sprintf("%s/%s", path_file, pt[i])
		}

		fileNameAndUnix := fmt.Sprintf("%d_%s", util.GetTimeNow().Unix(), image.Filename)

		// Destination
		dest := fmt.Sprintf("%s/%s", dir_file, fileNameAndUnix)
		dst, err := os.Create(dest)
		if err != nil {
			appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
			return
		}
		defer dst.Close()

		// Copy
		if _, err = io.Copy(dst, src); err != nil {
			appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
			return
		}

		var imageForm models.FileUpload
		// fileName := fmt.Sprintf("%s://%s/upload/%s", c.Scheme(), r.Host, fileNameAndUnix)
		imageForm.FileName = fileNameAndUnix
		imageForm.FilePath = fmt.Sprintf("%s/%s", path_file, fileNameAndUnix)
		imageForm.FileType = filepath.Ext(fileNameAndUnix)

		err = u.useSaFileUpload.CreateFileUpload(ctx, &imageForm)
		if err != nil {
			appE.ResponseError(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
			return
		}
		imageFormList = append(imageFormList, imageForm)

	}
	appE.Response(http.StatusOK, "Ok", imageFormList)

}
