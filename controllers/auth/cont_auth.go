package contauth

import (
	"context"
	"fmt"
	"net/http"

	iauth "gitlab.com/nuryanto2121/base-gin/interface/auth"
	"gitlab.com/nuryanto2121/base-gin/models"
	app "gitlab.com/nuryanto2121/base-gin/pkg/app"
	"gitlab.com/nuryanto2121/base-gin/pkg/logging"
	tool "gitlab.com/nuryanto2121/base-gin/pkg/tools"
	util "gitlab.com/nuryanto2121/base-gin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type ContAuth struct {
	useAuth iauth.Usecase
}

func NewContAuth(e *gin.Engine, useAuth iauth.Usecase) {
	cont := &ContAuth{
		useAuth: useAuth,
	}

	e.GET("/health_check", cont.Health)
	r := e.Group("/api/auth")
	// r.Use(midd.Versioning)
	r.POST("/login", cont.Login)
	r.POST("/forgot", cont.ForgotPassword)
	r.POST("/change_password", cont.ChangePassword)
	r.POST("/register", cont.Register)
	r.POST("/verify_otp", cont.VerifyOTP)

	L := e.Group("/api/auth/logout")
	// L.Use(midd.Authorize)
	L.POST("", cont.Logout)

}

func (u *ContAuth) Health(e *gin.Context) {
	e.JSON(http.StatusOK, "success")
}

// Verify :
// @Summary Verify the OTP Forgot Password Or OTP Login you have received in email, ok(OTP login go menu), ok (forgot password go change password)
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param req body models.VerifyForm true "req param #changes are possible to adjust the form of the registration form from frontend"
// @Success 200 {object} app.Response
// @Router /api/auth/verify_otp [post]
func (u *ContAuth) VerifyOTP(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var (
		logger = logging.Logger{}
		appE   = app.Gin{C: e}
		form   = models.VerifyForm{}
	)

	// validasi and bind to struct
	httpCode, errMsg := app.BindAndValid(e, &form)
	logger.Info(util.Stringify(form))
	if httpCode != 200 {
		appE.Response(http.StatusBadRequest, errMsg, nil)
		return
	}

	err := u.useAuth.Verify(ctx, form)
	if err != nil {
		appE.Response(http.StatusUnauthorized, fmt.Sprintf("%v", err), nil)
		return
	}
	appE.Response(http.StatusOK, "Ok", nil)
}

// Logout :
// @Summary logout
// @Security ApiKeyAuth
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Success 200 {object} app.Response
// @Router /api/auth/logout [post]
func (u *ContAuth) Logout(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var (
		appE = app.Gin{C: e} // wajib
	)

	claims, err := app.GetClaims(e)
	if err != nil {
		appE.Response(http.StatusBadRequest, fmt.Sprintf("%v", err), nil)
		return
	}
	Token := e.Request.Header.Get("Authorization")
	err = u.useAuth.Logout(ctx, claims, Token)
	if err != nil {
		appE.Response(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}

	appE.Response(http.StatusOK, "Ok", nil)
}

// Login :
// @Summary Login
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param req body models.LoginForm true "req param #changes are possible to adjust the form of the registration form from frontend"
// @Success 200 {object} app.Response
// @Router /api/auth/login [post]
func (u *ContAuth) Login(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var (
		logger = logging.Logger{}
		appE   = app.Gin{C: e}
		form   = models.LoginForm{}
	)

	// validasi and bind to struct
	httpCode, errMsg := app.BindAndValid(e, &form)
	logger.Info(util.Stringify(form))
	if httpCode != 200 {
		appE.Response(http.StatusBadRequest, errMsg, nil)
		return
	}

	out, err := u.useAuth.Login(ctx, &form)
	if err != nil {
		appE.Response(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}

	appE.Response(http.StatusOK, "Ok", out)
}

// ChangePassword :
// @Summary Change Password
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param req body models.ResetPasswd true "req param #changes are possible to adjust the form of the registration form from frontend"
// @Success 200 {object} app.Response
// @Router /api/auth/change_password [post]
func (u *ContAuth) ChangePassword(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var (
		logger = logging.Logger{} // wajib
		appE   = app.Gin{C: e}    // wajib
		// client sa_models.SaClient

		form = models.ResetPasswd{}
	)
	httpCode, errMsg := app.BindAndValid(e, &form)
	logger.Info(util.Stringify(form))
	if httpCode != 200 {
		appE.Response(http.StatusBadRequest, errMsg, nil)
		return
	}
	err := u.useAuth.ResetPassword(ctx, &form)
	if err != nil {
		appE.Response(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}

	appE.Response(http.StatusOK, "Ok", "Please Login")
}

// Register :
// @Summary Register
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param req body models.RegisterForm true "Body with file zip"
// @Success 200 {object} app.Response
// @Router /api/auth/register [post]
func (u *ContAuth) Register(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var (
		logger = logging.Logger{}
		appE   = app.Gin{C: e}
		form   = models.RegisterForm{}
	)

	// validasi and bind to struct
	httpCode, errMsg := app.BindAndValid(e, &form)
	logger.Info(util.Stringify(form))
	if httpCode != 200 {
		appE.Response(http.StatusBadRequest, errMsg, nil)
		return
	}

	data, err := u.useAuth.Register(ctx, form)
	if err != nil {
		appE.Response(http.StatusBadRequest, err.Error(), nil)
		return
	}

	appE.Response(http.StatusOK, "Ok", data)
}

// ForgotPassword :
// @Summary Forgot Password
// @Tags Auth
// @Produce json
// @Param Device-Type header string true "Device Type"
// @Param Version header string true "Version Apps"
// @Param req body models.ForgotForm true "req param #changes are possible to adjust the form of the registration form from frontend"
// @Success 200 {object} app.Response
// @Router /api/auth/forgot [post]
func (u *ContAuth) ForgotPassword(e *gin.Context) {
	ctx := e.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}
	var (
		logger = logging.Logger{} // wajib
		appE   = app.Gin{C: e}    // wajib
		// client sa_models.SaClient

		form = models.ForgotForm{}
	)
	// validasi and bind to struct
	httpCode, errMsg := app.BindAndValid(e, &form)
	logger.Info(util.Stringify(form))
	if httpCode != 200 {
		appE.Response(http.StatusBadRequest, errMsg, nil)
		return

	}

	err := u.useAuth.ForgotPassword(ctx, &form)
	if err != nil {
		appE.Response(tool.GetStatusCode(err), fmt.Sprintf("%v", err), nil)
		return
	}

	appE.Response(http.StatusOK, "Check Your Email", nil)

}
