package models

import (
	"errors"
)

var (
	ErrInternalServerError       = errors.New("internal_server_error") // ErrInternalServerError : will throw if any the Internal Server Error happen
	ErrNotFound                  = errors.New("data_not_found")        // ErrNotFound : will throw if the requested item is not exists
	ErrOtpNotFound               = errors.New("otp_not_found")         //ErrOtpNotFound
	ErrExpiredOtp                = errors.New("expired_otp")
	ErrConflict                  = errors.New("conflict")         // ErrConflict : will throw if the current action already exists
	ErrAccountConflict           = errors.New("conflict_account") // ErrConflict : will throw if the current action already exists
	ErrBadParamInput             = errors.New("bad_parameter")    // ErrBadParamInput : will throw if the given request-body or params is not valid
	ErrUnauthorized              = errors.New("un_authorized")
	ErrInvalidLogin              = errors.New("invalid_login")
	ErrAccountNotRegister        = errors.New("account_not_found")
	ErrVersioningNotFound        = errors.New("versioning_not_found")
	ErrVersioningHeaderNotFound  = errors.New("versioning_header_not_found")
	ErrUpdateYourApp             = errors.New("update_your_app")
	ErrForceUpdateYourApp        = errors.New("force_update_your_app")
	ErrWrongPassword             = errors.New("wrong_password")
	ErrWrongPasswordConfirm      = errors.New("password_and_confirm_not_same")
	ErrNoUploadFile              = errors.New("file_upload_not_found")
	ErrNoEmailCantForgotPassword = errors.New("no_email_cant_forgot_password")
	ErrFirstAccountNeeded        = errors.New("first_account_needed")
	ErrEmailNotFound             = errors.New("email_not_found")
)
