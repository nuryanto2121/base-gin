package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// import uuid "github.com/satori/go.uuid"

type Users struct {
	UserID uuid.UUID `json:"user_id" gorm:"primary_key;type:uuid;default:uuid_generate_v4()"`
	AddUser
	Model
}

type AddUser struct {
	Name      string    `json:"name" gorm:"type:varchar(60);not null"`
	PhoneNo   string    `json:"phone_no" gorm:"type:varchar(15)" cql:"phone_no"`
	Email     string    `json:"email" gorm:"type:varchar(60)"`
	IsActive  bool      `json:"is_active" gorm:"type:boolean"`
	JoinDate  time.Time `json:"join_date" gorm:"type:timestamp(0);default:now()"`
	Password  string    `json:"password" gorm:"type:varchar(150)"`
	FileID    int       `json:"file_id" gorm:"type:integer"`
	FilePatch string    `json:"file_path" gorm:"varchar(100)"`
}
