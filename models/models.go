package models

import "time"

// Model :
type Model struct {
	CreatedBy string    `json:"created_by" gorm:"type:varchar(60)"`
	UpdatedBy string    `json:"updated_by" gorm:"type:varchar(60)"`
	CreatedAt time.Time `json:"created_at" gorm:"type:timestamp with time zone;default:now()"`
	UpdatedAt time.Time `json:"updated_at" gorm:"type:timestamp with time zone;default:now()"`
}
