package postgres

import (
	"fmt"
	"log"
	"time"

	version "gitlab.com/nuryanto2121/base-gin/middleware/versioning"
	"gitlab.com/nuryanto2121/base-gin/models"
	"gitlab.com/nuryanto2121/base-gin/pkg/setting"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Conn *gorm.DB

func Setup() {
	now := time.Now()
	var err error

	connectionstring := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		setting.DatabaseSetting.Host,
		setting.DatabaseSetting.User,
		setting.DatabaseSetting.Password,
		setting.DatabaseSetting.Name,
		setting.DatabaseSetting.Port)
	fmt.Printf("%s", connectionstring)

	Conn, err = gorm.Open(postgres.Open(connectionstring), &gorm.Config{})

	if err != nil {
		log.Printf("connection.setup err : %v", err)
		panic(err)
	}

	sqlDB, err := Conn.DB()
	if err != nil {
		log.Printf("connection.setup DB err : %v", err)
		panic(err)
	}
	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(100)

	go autoMigratePool()

	timeSpent := time.Since(now)
	log.Printf("Config database is ready in %v", timeSpent)

}

func autoMigratePool() {
	// Add auto migrate bellow this line

	Trx, err := Conn.DB()
	if err != nil {
		log.Printf("connection.setup autoMigrate err : %v", err)
		panic(err)
	}
	// defer Trx.Close()
	rest, err := Trx.Exec(`
		CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
	`)

	log.Printf("%v", rest)
	if err != nil {
		log.Printf(" : %v", err)
		panic(err)
	}

	log.Println("STARTING AUTO MIGRATE ")
	Conn.AutoMigrate(
		models.Users{},
		version.AppVersion{},
		models.FileUpload{},
	)

	log.Println("FINISHING AUTO MIGRATE ")
}
