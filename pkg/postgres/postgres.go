package postgres

import (
	"fmt"
	"log"
	"time"

	version "gitlab.com/nuryanto2121/base-gin/middleware/versioning"
	"gitlab.com/nuryanto2121/base-gin/models"
	"gitlab.com/nuryanto2121/base-gin/pkg/setting"
	util "gitlab.com/nuryanto2121/base-gin/pkg/utils"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// var conn *gorm.DB

type PostgresDB struct {
	client *gorm.DB
	// Sql *sql.DB
}

func InitDB() *PostgresDB {
	now := time.Now()
	var err error

	connectionstring := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		setting.DatabaseSetting.Host,
		setting.DatabaseSetting.User,
		setting.DatabaseSetting.Password,
		setting.DatabaseSetting.Name,
		setting.DatabaseSetting.Port)
	fmt.Printf("%s", connectionstring)

	// newLogger := logger.New(
	// 	log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
	// 	logger.Config{
	// 		SlowThreshold: time.Second,   // Slow SQL threshold
	// 		LogLevel:      logger.Silent, // Log level
	// 		Colorful:      true,          // Disable color
	// 	},
	// )

	// conn, err := gorm.Open(postgres.Open(connectionstring), &gorm.Config{
	// 	NamingStrategy: schema.NamingStrategy{
	// 		TablePrefix:   setting.DatabaseSetting.TablePrefix,
	// 		SingularTable: true,
	// 	},
	// 	Logger: newLogger,
	// })
	conn, err := gorm.Open(postgres.Open(connectionstring), &gorm.Config{})

	if err != nil {
		log.Printf("connection.setup err : %v", err)
		panic(err)
	}

	// sqlDB, err := conn.DB()
	// if err != nil {
	// 	log.Printf("connection.setup DB err : %v", err)
	// 	panic(err)
	// }
	// sqlDB.SetMaxIdleConns(10)
	// sqlDB.SetMaxOpenConns(100)

	// go autoMigrate()

	timeSpent := time.Since(now)
	log.Printf("Config database is ready in %v", timeSpent)

	return &PostgresDB{
		client: conn,
	}
}
func (pb *PostgresDB) Close() error {
	trx, err := pb.client.DB()
	if err != nil {
		return err
	}
	return trx.Close()
}

// autoMigrate
func autoMigrate() {
	// Add auto migrate bellow this line
	conn := InitDB().client
	Trx, err := conn.DB()
	if err != nil {
		log.Printf("connection.setup autoMigrate err : %v", err)
		panic(err)
	}
	defer Trx.Close()
	rest, err := Trx.Exec(`
		CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
	`)

	log.Printf("%v", rest)
	if err != nil {
		log.Printf(" : %v", err)
		panic(err)
	}

	log.Println("STARTING AUTO MIGRATE ")
	conn.AutoMigrate(
		models.Users{},
		version.AppVersion{},
		models.FileUpload{},
	)

	log.Println("FINISHING AUTO MIGRATE ")
}

// updateTimeStampForCreateCallback will set `CreatedOn`, `ModifiedOn` when creating
func updateTimeStampForCreateCallback(db *gorm.DB) {
	if db.Statement.Error == nil {
		TimeInput := db.Statement.Schema.LookUpField("created_at")
		TimeInput.Set(db.Statement.ReflectValue, util.GetTimeNow())

		TimeEdit := db.Statement.Schema.LookUpField("updated_at")
		TimeEdit.Set(db.Statement.ReflectValue, util.GetTimeNow())
	}
}

// updateTimeStampForUpdateCallback will set `ModifiedOn` when updating
func updateTimeStampForUpdateCallback(db *gorm.DB) {
	if db.Statement.Changed() {
		db.Statement.SetColumn("updated_at", util.GetTimeNow())
	}

}
