package app

import (
	"errors"

	"github.com/astaxie/beego/validation"

	"gitlab.com/nuryanto2121/base-gin/pkg/logging"
)

// MarkErrors logs error logs
func MarkErrors(errors []*validation.Error) {
	for _, err := range errors {
		logging.Info(err.Key, err.Message)
	}

	return
}

func ErrorString(message string) error {
	return errors.New(message)
}
