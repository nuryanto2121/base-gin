package app

import (
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/nuryanto2121/base-gin/models"
	util "gitlab.com/nuryanto2121/base-gin/pkg/utils"
)

// BindAndValid binds and validates data
func BindAndValid(c *gin.Context, form interface{}) (int, string) {
	err := c.Bind(form)
	if err != nil {
		return http.StatusBadRequest, models.ErrBadParamInput.Error()
	}

	valid := validation.Validation{}
	check, err := valid.Valid(form)
	if err != nil {
		return http.StatusInternalServerError, models.ErrInternalServerError.Error()
	}
	if !check {
		MarkErrors(valid.Errors)
		return http.StatusBadRequest, models.ErrBadParamInput.Error()
	}

	return http.StatusOK, "ok"
}

// GetClaims :
func GetClaims(c *gin.Context) (util.Claims, error) {
	var clm util.Claims
	claims := c.GetStringMapString("claims")

	err := mapstructure.Decode(claims, &clm)
	if err != nil {
		return clm, err
	}

	return clm, nil
}
