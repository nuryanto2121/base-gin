package useauth

import (
	"context"
	"time"

	"github.com/mitchellh/mapstructure"
	iauth "gitlab.com/nuryanto2121/base-gin/interface/auth"
	ifileupload "gitlab.com/nuryanto2121/base-gin/interface/fileupload"
	iusers "gitlab.com/nuryanto2121/base-gin/interface/user"
	"gitlab.com/nuryanto2121/base-gin/models"
	"gitlab.com/nuryanto2121/base-gin/pkg/app"
	"gitlab.com/nuryanto2121/base-gin/pkg/redisdb"
	util "gitlab.com/nuryanto2121/base-gin/pkg/utils"

	useemailauth "gitlab.com/nuryanto2121/base-gin/usecase/email_auth"
)

type useAuht struct {
	repoAuth       iusers.Repository
	repoFile       ifileupload.Repository
	contextTimeOut time.Duration
}

func NewUserAuth(a iusers.Repository, b ifileupload.Repository, timeout time.Duration) iauth.Usecase {
	return &useAuht{repoAuth: a, repoFile: b, contextTimeOut: timeout}
}
func (u *useAuht) Login(ctx context.Context, dataLogin *models.LoginForm) (output interface{}, err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeOut)
	defer cancel()

	dataUser, err := u.repoAuth.GetByAccount(ctx, dataLogin.Account)
	if err != nil {
		return nil, err
	}
	if !util.ComparePassword(dataUser.Password, util.GetPassword(dataLogin.Password)) {
		return nil, app.ErrorString("Invalid Password")
	}

	if !dataUser.IsActive {
		return nil, app.ErrorString("Account not active")
	}

	token, err := util.GenerateToken(dataUser.UserID.String(), dataUser.Name, "")
	if err != nil {
		return nil, err
	}

	redisdb.AddSession(token, dataUser.UserID, time.Duration(24)*time.Hour)

	response := map[string]interface{}{
		"token": token,
		"name":  dataUser.Name,
	}
	return response, nil
}

func (u *useAuht) ForgotPassword(ctx context.Context, dataForgot *models.ForgotForm) (err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeOut)
	defer cancel()

	dataUser, err := u.repoAuth.GetByAccount(ctx, dataForgot.Account)
	if err != nil {
		return err
	}

	GenCode := util.GenerateNumber(4)

	// send generate code
	mailService := &useemailauth.Register{
		Email:      dataUser.Email,
		Name:       dataUser.Name,
		GenerateNo: GenCode,
	}

	go mailService.SendRegister()

	//store to redis
	err = redisdb.AddSession(dataUser.Email, GenCode, 5*time.Minute)
	if err != nil {
		return err
	}

	return nil
}

func (u *useAuht) ResetPassword(ctx context.Context, dataReset *models.ResetPasswd) (err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeOut)
	defer cancel()

	if dataReset.Passwd != dataReset.ConfirmPasswd {
		return models.ErrWrongPasswordConfirm
	}

	dataUser, err := u.repoAuth.GetByAccount(ctx, dataReset.Account)
	if err != nil {
		return err
	}

	dataUser.Password, _ = util.Hash(dataReset.Passwd)
	dtUpdate := map[string]interface{}{
		"password": dataUser.Password,
	}
	err = u.repoAuth.Update(ctx, dataUser.UserID, dtUpdate)
	if err != nil {
		return err
	}
	return nil
}

func (u *useAuht) Register(ctx context.Context, dataRegister models.RegisterForm) (output interface{}, err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeOut)
	defer cancel()

	var User models.Users

	dataUser, err := u.repoAuth.GetByAccount(ctx, dataRegister.Email)
	if err != nil && err != models.ErrNotFound {
		return nil, err
	}

	if dataUser.Email != "" {
		return nil, app.ErrorString("Account already exist.")
	}

	if dataRegister.Password != dataRegister.ConfirmPassword {
		return output, app.ErrorString("Password and Confirm Password Not Valid")
	}

	err = mapstructure.Decode(dataRegister, &User.AddUser)
	if err != nil {
		return output, err
	}
	User.Password, _ = util.Hash(dataRegister.Password)
	User.CreatedBy = dataRegister.Name
	User.UpdatedBy = dataRegister.Name
	User.IsActive = false

	err = u.repoAuth.Create(ctx, &User)
	if err != nil {
		return output, err
	}

	GenCode := util.GenerateNumber(4)

	// send generate code
	mailService := &useemailauth.Register{
		Email:      User.Email,
		Name:       User.Name,
		GenerateNo: GenCode,
	}

	err = mailService.SendRegister()
	if err != nil {
		return output, err
	}

	//store to redis
	err = redisdb.AddSession(User.Email, GenCode, 5*time.Minute)
	if err != nil {
		return output, err
	}
	out := map[string]interface{}{
		"gen_code": GenCode,
	}
	return out, nil
}

func (u *useAuht) Verify(ctx context.Context, dataVerify models.VerifyForm) (err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeOut)
	defer cancel()

	dataUser, err := u.repoAuth.GetByAccount(ctx, dataVerify.Account)
	if err != nil && err != models.ErrNotFound {
		return err
	}

	dataCode := redisdb.GetSession(dataVerify.Account)
	if dataCode == "" {
		return app.ErrorString("Please Resend Code")
	}

	if dataCode != dataVerify.VerifyCode {
		return app.ErrorString("Invalid Code.")
	}

	dtUpdate := map[string]interface{}{
		"is_active": true,
	}
	err = u.repoAuth.Update(ctx, dataUser.UserID, dtUpdate)
	if err != nil {
		return err
	}

	return nil
}

func (u *useAuht) Logout(ctx context.Context, Claims util.Claims, Token string) (err error) {
	return nil
}
