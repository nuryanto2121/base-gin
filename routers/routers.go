package routers

import (
	"time"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	_ "gitlab.com/nuryanto2121/base-gin/docs"
	"gitlab.com/nuryanto2121/base-gin/pkg/postgres"
	"gitlab.com/nuryanto2121/base-gin/pkg/setting"

	_contAuth "gitlab.com/nuryanto2121/base-gin/controllers/auth"

	_repoUser "gitlab.com/nuryanto2121/base-gin/repository/user"
	_useAuth "gitlab.com/nuryanto2121/base-gin/usecase/auth"

	_contFileUpload "gitlab.com/nuryanto2121/base-gin/controllers/fileupload"
	_repoFileUpload "gitlab.com/nuryanto2121/base-gin/repository/fileupload"
	_useFileUpload "gitlab.com/nuryanto2121/base-gin/usecase/fileupload"
)

type GinRoutes struct {
	G *gin.Engine
}

func (g *GinRoutes) Init() {
	timeoutContext := time.Duration(setting.ServerSetting.ReadTimeout) * time.Second

	r := g.G
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	repoFileUpload := _repoFileUpload.NewRepoFileUpload(postgres.Conn)
	useFileUpload := _useFileUpload.NewSaFileUpload(repoFileUpload, timeoutContext)

	repoUser := _repoUser.NewRepoSysUser(postgres.Conn)
	useAuth := _useAuth.NewUserAuth(repoUser, repoFileUpload, timeoutContext)

	_contAuth.NewContAuth(g.G, useAuth)

	_contFileUpload.NewContFileUpload(g.G, useFileUpload)

}

// func InitRouter() *gin.Engine {
// 	timeoutContext := time.Duration(setting.ServerSetting.ReadTimeout) * time.Second
// 	r := gin.New()
// 	r.Use(gin.Logger())
// 	r.Use(gin.Recovery())
// 	r.Use(cors.Default())

// 	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
// 	repoFile := _repoFile.NewRepoFileUpload(postgres.Conn)

// 	repoUser := _repoUser.NewRepoSysUser(postgres.Conn)
// 	useAuth := _useAuth.NewUserAuth(repoUser, repoFile, timeoutContext)

// 	_contAuth.NewContAuth(r, useAuth)
// 	return r
// }
